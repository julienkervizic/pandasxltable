"""
Copyright (c) 2019 Julien Kervizic

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from typing import List
import openpyxl
from pandasxltable.xl_table import XLTable


class XLWB():
    def __init__(self):
        self._path = None
        self._wb = None

    def __repr__(self):
        return f"XLWB()"

    def from_path(self, path: str):
        """
        Initialize the XLWB object from a path
        """
        self._path = path
        self._wb = openpyxl.load_workbook(path)
        return self

    @property
    def wb(self):
        return self._wb

    def list_tables(self) -> List:
        """
        List all tablees objects contained in the worksheet
        """
        
        wb = self.wb
        sheet_names = wb.get_sheet_names()
        sheets = [wb.get_sheet_by_name(sheet_name) for sheet_name in sheet_names]
        tables_not_flattened = [(sheet, sheet._tables) for sheet in sheets if sheet._tables]
        tables = [
            XLTable().from_openpyxl_table(x[0], table)
            for x in tables_not_flattened for table in x[1]
        ]
        return tables

    def get_table_by_name(self, name) -> XLTable:
        """
        Return the XLTable object with a given name
        """
        table = [tbl for tbl in self.list_tables() if tbl.name == name] 
        return table[0] if len(table) > 0 else None

    def save(self):
        self._wb.save(self.path)

    def save_as(self, filename):
        self._wb.save(filename)