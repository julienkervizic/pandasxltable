"""
Copyright (c) 2019 Julien Kervizic

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from typing import AnyStr, Tuple
from openpyxl.utils import coordinate_to_tuple, get_column_letter


class XLRange():
    """
    Utility class to manage operations on xl ranges
    """
    def __init__(self):
        self._nw = None
        self._ne = None
        self._sw = None
        self._se = None
        self._xl_ref = None

    def __repr__(self) -> str:
        return f"XLRange({self._xl_ref})"

    @property
    def nw(self) -> Tuple:
        """
        tuple: Returns the coordinate in the north west corner of the range
        """
        return self._nw
    
    @property
    def ne(self) -> Tuple:
        """
        tuple: Returns the coordinate in the north east corner of the range
        """
        return self._ne
    
    @property
    def sw(self) -> Tuple:
        """
        tuple: Returns the coordinate in the south west corner of the range
        """
        return self._sw
    
    @property
    def se(self) -> Tuple:
        """
        tuple: Returns the coordinate in the south west corner of the range
        """
        return self._se
    
    @property
    def xl_ref(self) -> AnyStr:
        """
        Retunrs the xl reference for the range
        example: A1:B2
        """
        return self._xl_ref

    def from_xl_reference(self, xl_ref):
        """
        Initialize the XLRange object from an XL reference
        eg: A1:B2
        """
        init, end = xl_ref.split(':')
        self._xl_ref = xl_ref
        self._nw = coordinate_to_tuple(init)
        self._se = coordinate_to_tuple(end)
        self._ne = (self._nw[0], self._se[1])
        self._sw = (self._se[0], self._nw[1])
        return self

    def from_coordinates(self, init_coo: tuple, end_coo: tuple):
        """
        Initialize the XLRange object from two coordinates
        eg: (2,1) and (2, 2) -> XLRange(A2:B2)
        """
        start_num_row, start_num_col = init_coo
        end_num_row, end_num_col = end_coo
        start_col_letter = get_column_letter(start_num_col)
        end_col_letter = get_column_letter(end_num_col)
        xl_ref = start_col_letter + str(start_num_row)
        xl_ref += ":" + end_col_letter + str(end_num_row)
        return self.from_xl_reference(xl_ref)
    
    def add_columns_right(self, num_cols: int):
        """
        Add a number of columns to the right of the range
        example: XLRange(A2:B2) + 3 cols -> XLRange(A2:E2)
        """
        nw, se = self.nw, self.se
        new_se = (se[0], se[1] + num_cols)
        return self.from_coordinates(nw, new_se)
    
    def add_columns_left(self, num_cols: int):
        """
        Add a number of columns to the left of the range
        example: XLRange(C2:D2) + 2 cols -> XLRange(A2:D2)
        """
        nw, se = self.nw, self.se
        new_nw_col_pos = self.nw[1] - num_cols
        if new_nw_col_pos < 1:
            raise ValueError("Columns number should be > 1")
        if new_nw_col_pos > se[1]:
            raise ValueError("Initial column number (nw) cannot be greater than end column number (se)")
        new_nw = (self.nw[0], new_nw_col_pos)
        return self.from_coordinates(new_nw, se)

    def add_rows_bottom(self, num_rows: int):
        """
        Add a number of rows at the end of the range
        example: XLRange(C2:B2) + 2 rows -> XLRange(A2:B4)
        """
        nw, se = self.nw, self.se
        new_se = (se[0] + num_rows, se[1])
        return self.from_coordinates(nw, new_se)

    def add_rows_top(self, num_rows: int):
        """
        Add a number of rows at the top of the range
        example: XLRange(C2:B2) + 1 rows -> XLRange(A1:B2)
        """
        nw, se = self.nw, self.se
        new_nw_row_pos = nw[0] - num_rows
        if new_nw_row_pos < 1:
            raise ValueError("Row number should be > 1")
        if new_nw_row_pos > se[0]:
            raise ValueError("Initial row number (nw) cannot be greater than end row number (se)")
        new_nw = (new_nw_row_pos, nw[1])
        return self.from_coordinates(new_nw, se)

    def remove_columns_right(self, num_cols: int):
        """
        Returns an XLRange object with the n column removed from the right
        """
        return self.add_columns_right(-num_cols)

    def remove_columns_left(self, num_cols: int):
        """
        Returns an XLRange object with the n column removed from the left
        """        
        return self.add_columns_left(-num_cols)

    def remove_rows_bottom(self, num_rows: int):
        """
        Returns an XLRange object with the bottom n rows removed from the range
        """
        return self.add_rows_bottom(-num_rows)

    def remove_rows_top(self, num_rows: int):
        """
        Returns an XLRange object with the top n rows removed from the range
        """
        return self.add_rows_top(-num_rows)

    @property
    def row_cnt(self) -> int:
        """
        Returns the number of rows contained in the range
        """
        row_cnt = self.sw[0] - self.nw [0] + 1
        return row_cnt

    @property
    def column_cnt(self) -> int:
        """
        Returns the number of columns contained in the range
        """
        column_cnt = self.ne[1] - self.nw[1] +1
        return column_cnt
    
    @property
    def top_row_ref(self) -> AnyStr:
        """
        Returns the excel reference for the top row of the range
        """
        return self.from_coordinates(self.nw, self.ne).xl_ref