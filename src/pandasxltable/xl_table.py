"""
Copyright (c) 2019 Julien Kervizic

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from typing import List, AnyStr
import pandas as pd
from pandas.core.frame import DataFrame as DataFrameType
from openpyxl.worksheet.table import TableColumn, Table, TableStyleInfo
from openpyxl.worksheet.table import Table as TableType
from openpyxl.worksheet.worksheet import Worksheet as WorkSheetType
from pandasxltable.xl_range import XLRange


class XLTable():
    def __init__(self):
        self._sheet = None        
        self._table = None

    def from_openpyxl_table(self, sheet: WorkSheetType, tbl: TableType):
        """
        Instantiate the object based on an OpenPyXL Table and a Worksheet
        :param WorkSheetType sheet:
        :param TableType  tbl:
        """
        self._sheet = sheet
        self._table = tbl
        return self

    def __repr__(self):
        return f"XLTable({self.name})"

    @property
    def name(self) -> AnyStr:
        return self._table.name

    @property
    def sheet(self) -> WorkSheetType:
        return self._sheet

    @sheet.setter
    def sheet(self, sheet):
        self._sheet = sheet

    @property
    def ref(self) -> AnyStr:
        return self._table.ref

    @ref.setter
    def ref(self, ref):
        self._table.ref = ref

    @property
    def xl_range(self) -> XLRange:
        return XLRange().from_xl_reference(self._table.ref)
    
    @property
    def table_columns(self) -> List:
        """
        Retunrs thhe OpenPyXL tableColumns object contained in a data table
        :return List:
        """
        return self._table.tableColumns

    @table_columns.setter
    def table_columns(self, tbl_columns):
        self._table.tableColumns = tbl_columns

    @property
    def table_column_names(self) -> List:
        """
        Return the names of the header columns in the datatable
        :return List[AnyStr]:
        """
        return [tbl.name for tbl in self.table_columns]
    
    @property
    def row_cnt(self) -> int:
        """
        Number of rows contained in the table object
        """
        xl_range= self.xl_range
        return xl_range.row_cnt

    @property
    def formula_columns(self):
        formula_columns = [
            (x, table_column) 
            for x, table_column 
            in enumerate(self.table_columns) 
            if table_column.calculatedColumnFormula
            ]
        return formula_columns

    def _delete_values(self) -> None:
        """
        Deletes all values contained in the data table referenced range
        """
        xl_range_ref = self.xl_range.xl_ref
        for row in self.sheet[xl_range_ref]:
            for cell in row:
                cell.value = None

    def _update_headers(self, column_names: List) -> None:
        """
        Resize and update the headers of a datatable from a list of
        column names

        """
        # check the number of columsn
        net_columns = len(column_names) - self.xl_range.column_cnt
        # create TableColumn Objects based on the dataframes
        tbl_columns = [
            TableColumn(id=k + 1, name=v)
            for k, v
            in enumerate(column_names)
        ]
        self.table_columns = tbl_columns        
        # resize columns range
        new_range = self.xl_range.add_columns_right(net_columns)
        self.ref = new_range.xl_ref
        # updating sheet values
        for row in self.sheet[new_range.top_row_ref]:
            u = 0
            for cell in row:
                cell.value = column_names[u]
                u += 1

    def _update_data_from_df(self, df: DataFrameType) -> None:
        """
        Internal function to update data from a DataFrameType
        The function populate rows based based on thhe dataframe values
        """
        # resize rows
        net_rows = df.shape[0] - self.xl_range.row_cnt
        new_range = self.xl_range.add_rows_bottom(net_rows + 1) # +1 for headers
        self.ref = new_range.xl_ref
        # update _values
        data_range = new_range.remove_rows_top(1)
        data_rows = df.values
        i = 0 
        for row in self.sheet[data_range.xl_ref]:
                row_values = data_rows[i]
                i += 1
                u = 0
                for cell in row:
                    cell.value = row_values[u]
                    u += 1

    def create_from_dataframe(
            self,
            sheet: WorkSheetType,
            ref: AnyStr,
            df: DataFrameType
        ):
        """
        Create an Excel datatable in the Worksheet based on a dataframe

        :param WorkSheetType sheet: The workssheet in which the Excel Data table will be created
        :param AnyStr ref: The initial Excel range reference (ie "A1:B4 ") from which the data will be populated
        :param DataFrameType df: The input dataframe used to populate the Excel Table
        :raise TypeError: if inputs are of an unexpected type
        :return: self
        """
        if not isinstance(df, DataFrameType): raise TypeError("Expected a Dataframe")
        if not isinstance(ref, str): raise TypeError("Expected a string")
        if not isinstance(sheet, WorkSheetType): raise TypeError("Expected an OpenPyXL WorkSheet")
        try:
            df.name
        except Exception:
            raise TypeError("Expected a named dataframe")
        self._sheet = sheet
        self._table = Table(
            name=df.name,
            displayName=df.name,
            ref=ref,
            headerRowCount=1
        )
        # get expected range and clear the damn thing
        self._update_headers(df.columns)
        # update values
        self._update_data_from_df(df)
        style = TableStyleInfo(
            name="TableStyleMedium9",
            showFirstColumn=False,
            showLastColumn=False,
            showRowStripes=True,
            showColumnStripes=True
        )
        self._table.tableStyleInfo = style
        self._sheet.add_table(self._table)
        return self

    def update_from_dataframe(self, df):
        """
        Update the XL Table based on a dataframe input
        """
        # check that the _table object is instantiated
        # delete full range
        self._delete_values()
        # update columns
        self._update_headers(df.columns)
        # update values
        self._update_data_from_df(df)
        return self

    def to_dataframe(self) -> DataFrameType:
        """
        Converts an Excel Table object into a pandas dataframe
        :return DataFrameType: The converted object values as a dataframe
        """
        cell_range = self.sheet[self.ref]
        table_columns = self.table_columns

        df = (
            pd.DataFrame()
            .from_records(
                data=list(cell_range),
                columns=[column.name for column in table_columns]
            )
            .applymap(lambda x: x.value)
            [1:] #skip the header row
        )
        return df