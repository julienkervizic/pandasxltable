Pandasxltable
=====
Pandasxltable v0.1
Pandas Xl Table is a wrapper around openpyxl to facilitate the interaction between pandas dataframe and excel data tables.
The current version is a rough prototype and possibly subject to a lot of monstruous bugs.


Install 
=====
```
python3 setup.py install
```

commands
=====
List existing tables in path
-----
```
from pandasxltable import XLWB
wb = XLWB().from_path(p)
wb.list_tables()
```

Read the content of an excel datatable as a pandas dataframe
-----
```
import pandas as pd
tooble = wb.list_tables()[0]
tooble.to_dataframe()
```

Update table with a dataframe
-----
```
import pandas as pd
tooble = wb.list_tables()[0]
df = pd.DataFrame([[0,1], [9,0]], columns=["hi", "ho"])
tooble.update_from_dataframe(df)
wb.save_as("test.xlsx")
```

License
=====
MIT © Julien Kervizic 
